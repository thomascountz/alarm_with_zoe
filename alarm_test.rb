require 'test/unit'
require_relative './alarm'
require_relative './sensor'

class AlarmTest < Test::Unit::TestCase
  def tests_that_alarm_is_an_Alarm
    assert_equal(2, 1 + 1)
  end

  def test_sensor_is_Sensor
    test_sensor = Sensor.new
    alarm = Alarm.new(test_sensor)
    assert_equal(test_sensor.class, Sensor)
  end

  def test_alarm_on_is_initialized_as_false
    alarm = Alarm.new
    assert_equal(alarm.alarm_on, false)
  end

  def test_check_turns_on_alarm_when_pressure_too_low
    alarm = Alarm.new
    alarm.check(15)
    assert_equal(alarm.alarm_on, true)
  end

  def test_check_turns_on_alarm_when_pressue_too_high
    alarm = Alarm.new
    alarm.check(22)
    assert_equal(alarm.alarm_on, true)
  end

  def test_alarm_stays_off_when_appropriate_pressure
    alarm = Alarm.new
    alarm.check(20)
    assert_equal(alarm.alarm_on, false)
  end

end